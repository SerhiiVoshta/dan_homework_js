"use strict";

let tabs = document.querySelector('.tabs');

tabs.addEventListener('click', function (e) {
    let tab = e.target.closest('.tabs [data-name]');
    let activeEl = document.querySelectorAll('.centered-content [data-active=true]');
    activeEl.forEach((el) => el.dataset.active = 'false');
    let content = document.querySelector(`.tabs-content [data-name=${tab.dataset.name}]`);
    if (content) {
        content.dataset.active = 'true';
        tab.dataset.active = 'true';
    }
});
