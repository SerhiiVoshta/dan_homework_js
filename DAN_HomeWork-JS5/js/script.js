"use strict";

function translateDate(dateBirth) {
  if (dateBirth.length === 10) {
    let transDate = dateBirth.replace(/\,|\s|\./g, '-');
    if(
      Date.parse(transDate) &&
      Date.parse(transDate) < Date.now() &&
      transDate[4] ==='-' &&
      transDate[7] ==='-'
    ){
        return transDate;
    } else{
        transDate = dateBirth.slice(-4) + "-" + dateBirth.slice(3, 5) + "-" + dateBirth.slice(0, 2);
        if (Date.parse(transDate) && Date.parse(transDate) < Date.now()) {
            return transDate;
          } else {
              return false;
            }
    }
  } else {
    return false;
  }
}

function createNewUserFn() {
  let userName = prompt('введите имя');
  let userLastName = prompt('введите фамилию');
  let userBirthday = translateDate(prompt("введите дату рождения в формате dd.mm.yyyy (или yyyy.mm.dd)"));
  while (
    !userName ||
    !userLastName ||
    !userName.trim() ||
    !userLastName.trim() ||
    !userBirthday
  ) {
    userName = prompt("введите имя", userName);
    userLastName = prompt("введите фамилию", userLastName);
    userBirthday = translateDate(prompt("введите дату рождения в формате dd.mm.yyyy (или yyyy.mm.dd)"));
  }
  const newUser = {
    firstName: userName,
    lastName: userLastName,
    birthday: userBirthday,
    getLogin() {
      let login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      return login;
    },
    getPassword() {
      let password =
        this.firstName[0].toUpperCase() +  this.lastName.toLowerCase() + new Date(Date.parse(this.birthday)).getFullYear();
      return password;
    },
    getAge() {
      let age = parseInt((Date.now() - new Date(Date.parse(this.birthday))) /(1000 * 3600 * 24 * 365.25),10);
      return age;
    },
    setFirstName(uFirstName) {
      if (uFirstName && uFirstName.trim()) {
        Object.defineProperty(newUser, "firstName", { value: uFirstName });
      }
    },
    setLastName(uLastName) {
      if (uLastName && uLastName.trim()) {
        Object.defineProperty(newUser, "lastName", { value: uLastName });
      }
    },
  };
  Object.defineProperties(newUser, {
    firstName: { writable: false, configurable: true },
    lastName: { writable: false, configurable: true },
  });

  return newUser;
}

let user = createNewUserFn();
console.log(user);
console.log(`First Name: ${user.firstName}`);
console.log(`Last Name: ${user.lastName}`);

console.log(`Birthday: ${user.birthday}`);
console.log(`Age: ${user.getAge()}`);

console.log(`login: ${user.getLogin()}`);
console.log(`Password: ${user.getPassword()}`);
