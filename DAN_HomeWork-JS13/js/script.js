"use strict";

let nameBtnStorage;
let themeStorage;
let logo = document.querySelector('.logo-wrap');
let btnWrap = document.createElement('div');
btnWrap.setAttribute('class', 'buttons-wrap');
logo.insertAdjacentElement('afterend', btnWrap);

let btnTheme = document.createElement('button');
btnTheme.classList.add('class', 'btnTheme');
btnTheme.setAttribute('type', 'button');
btnTheme.setAttribute('data-color', 'green');
btnTheme.setAttribute('data-hover', 'green');
btnTheme.textContent = 'Blue Theme';
btnWrap.insertAdjacentElement('afterbegin', btnTheme);

let elementsColor = document.querySelectorAll('[data-color],[data-hover]');
let body = document.querySelector('.page-wrapper');

window.onload = function () {
    if (localStorage.getItem('theme') !== null) {
        themeStorage = localStorage.getItem('theme');
        body.dataset.bodyBg = themeStorage;
        elementsColor.forEach(el => {
            el.dataset.color = themeStorage;
            el.dataset.hover = themeStorage;
        });
    }
    if (localStorage.getItem('nameBtn') !== null) {
        nameBtnStorage = localStorage.getItem('nameBtn');
        btnTheme.textContent = nameBtnStorage;
    }

    document.addEventListener('click', function (e) {
        if (e.target === btnTheme) {

            if (e.target.dataset.color === 'blue') {
                btnTheme.textContent = 'Blue Theme';
                localStorage.setItem('theme', 'green');
                localStorage.setItem('nameBtn', 'Blue Theme');
                body.dataset.bodyBg = 'green';
                elementsColor.forEach(el => {
                    el.dataset.color = 'green';
                    el.dataset.hover = 'green';
                });

            } else if (e.target.dataset.color === 'green') {
                btnTheme.textContent = 'Green Theme';
                localStorage.setItem('theme', 'blue');
                localStorage.setItem('nameBtn', 'Green Theme');
                body.dataset.bodyBg = 'blue';
                elementsColor.forEach(el => {
                    el.dataset.color = 'blue';
                    el.dataset.hover = 'blue';
                });
            }
            // window.location.reload();
            e.preventDefault();
        }
        else return
    });
}
