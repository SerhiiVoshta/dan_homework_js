_Теоретический вопрос_

Почему для работы с input не рекомендуется использовать события клавиатуры?

_Ответ_

Не рекомендуется использовать события клавиатуры, т.к. ввод в поле input может быть не только с клавиатуры, но и с помощью мыши(copy/paste) или распознавания речи. 

