"use strict";

let buttons = [...document.querySelectorAll('[data-code]')];

document.addEventListener('keydown', function (e) {
    console.log(e);
    buttons.forEach(el => {
        if (e.code === el.dataset.code || e.key === el.dataset.code) {
            el.focus();
        } else if (e.key !== 'Shift') {
            el.blur();
        }
    });
})