"use strict";

let array = [1, 2.3, 3, 'q', 'qwer', { obj: 1 }, undefinedFn(), !!null, null, 0, false, 'true', -3, Symbol(), 1n];
debugger;
function undefinedFn() {
};

let type = typeSelect();
let arrayFiltered = filterBy(array, type);

function typeSelect() {
  let typeUser;
  do {
    typeUser = prompt(`Для выбора типа данных введите соответствующую цифру \n 1 - number\n 2 - string\n 3 - boolean\n 4 - object\n 5 - undefined\n 6 - bigint\n 7 - symbol\n`);
  } while (!typeUser || isNaN(+typeUser) || typeUser < 1 || typeUser > 7);

  let res;
  switch (+typeUser.trim()) {
    case 1: res = 'number'
      break;
    case 2: res = 'string'
      break;
    case 3: res = 'boolean'
      break;
    case 4: res = 'object'
      break;
    case 5: res = 'undefined'
      break;
    case 6: res = 'bigint'
      break;
    case 7: res = 'symbol'
      break;
    default:
      break;
  };
  return res;
}

function filterBy(array, dataType) {
  if (Array.isArray(array)) {
    let arrFiltered = array.filter((el) => typeof el !== dataType);
    return arrFiltered;
  } else {
    console.log(`Error! It's not array`);
    return false;
  }
};

if (arrayFiltered) {
  console.log(`Исходный массив:`);
  console.log(array);
  console.log('');
  console.log(`Массив без элементов типа ${type}:`);
  console.log(arrayFiltered);
}