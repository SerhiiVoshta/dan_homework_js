"use strict";

let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let div = document.createElement('div');
document.body.insertAdjacentElement("afterbegin", div)

function createList(arr, container = document.body) {
  let ul = document.createElement('ul');
  let listArr = arr.map((el) => {
    ul.innerHTML += `<li>${el}</li>`;
  });
  container.append(ul);
}

createList(array, div);

