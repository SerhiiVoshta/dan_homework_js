"use strict";

let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let div = document.createElement('div');
document.body.insertAdjacentElement("afterbegin", div)

function createList(arr, container = document.body) {
    let ul = document.createElement('ul');
    let listArr = arr.map((el) => el = `<li>${el}</li>`)
    ul.innerHTML = listArr.join('');
    container.append(ul);
}

createList(array, div);
