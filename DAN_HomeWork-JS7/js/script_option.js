"use strict";

let array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
let div = document.createElement('div');
document.body.insertAdjacentElement("afterbegin", div)

function createList(arr, container = document.body) {
    let ul = document.createElement('ul');
    let listArr = arr.map((el) => {
        if (Array.isArray(el)) {
            createList(el, ul);
        } else {
            ul.innerHTML += `<li>${el}</li>`;
        }
    })
    container.append(ul);
}
createList(array, div);

let timerContainer = document.createElement('div');
document.body.insertAdjacentElement('afterbegin', timerContainer);
let spanTimer = document.createElement('span');
timerContainer.insertAdjacentElement('afterbegin', spanTimer);
timerContainer.style.cssText = `text-align: center`;
spanTimer.style.cssText = `color: red; font-size: 55px; font-weight: bold`
spanTimer.textContent = 15;
timer();

function timer() {
    setTimeout(function start() {
        spanTimer.textContent = spanTimer.textContent - 1;
        if (spanTimer.textContent > 0) {
            setTimeout(start, 1000);
        } else {
            document.body.innerHTML = '';
        }
    }, 1000);
}

