"use strict";

function createNewUserFn(){
    let userName = prompt('введите имя');
    let userLastName = prompt('введите фамилию');
    while(!userName ||
        !userLastName ||
        !userName.trim() ||
        !userLastName.trim()){
        userName = prompt('введите имя', userName);
        userLastName = prompt('введите фамилию', userLastName);
    }
    const newUser = {
        firstName: userName,
        lastName: userLastName,
        getLogin(){
            let login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            return login;
        },
        setFirstName(uFirstName){
            if(uFirstName && uFirstName.trim()){
            Object.defineProperty (newUser, 'firstName', {value:uFirstName});
            } 
        },
        setLastName(uLastName){
            if(uLastName && uLastName.trim()){
            Object.defineProperty (newUser, 'lastName', {value:uLastName});
            } 
        },
    }
    Object.defineProperties(newUser,{ 
        firstName:{writable:false, configurable: true},
        lastName:{writable:false, configurable: true},
        });

    return newUser;
}

let user = createNewUserFn();

// user.setFirstName('Serhii'); /* проверка с помощью setFirstName() */
// user.setLastName('Forexample'); /* проверка с помощью setLastName() */

// user.firstName = 'Serhii'; /* проверка на изменение firstName*/
// user.lastName = 'Forexample'; /* проверка на изменение lastName*/

console.log(`First Name: ${user.firstName}`);
console.log(`Last Name: ${user.lastName}`);
console.log(`login: ${user.getLogin()}`);
