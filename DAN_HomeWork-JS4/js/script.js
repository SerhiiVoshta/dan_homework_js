"use strict";

function createNewUserFn(){
    let userName = prompt('введите имя');
    let userLastName = prompt('введите фамилию');
    while(!userName ||
        !userLastName ||
        !userName.trim() ||
        !userLastName.trim()){
        userName = prompt('введите имя', userName);
        userLastName = prompt('введите фамилию', userLastName);
    }
    const newUser = {
        firstName: userName,
        lastName: userLastName,
        getLogin(){
            let login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            return login;
        },
    }
    return newUser;
}

let user = createNewUserFn();

console.log(`First Name: ${user.firstName}`);
console.log(`Last Name: ${user.lastName}`);
console.log(`login: ${user.getLogin()}`);
