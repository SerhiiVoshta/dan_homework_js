"use strict";

let container = document.createElement('div');
container.setAttribute('id', 'container');
document.body.insertAdjacentElement('afterbegin', container);

let currentPrice = document.createElement('span');
currentPrice.setAttribute('id', 'currentPrice');
let inputWrap = document.createElement('div');
container.append(currentPrice, inputWrap);

let title = document.createElement('span');
title.innerHTML = 'Price, $:';
let input = document.createElement('input');
input.setAttribute('id', 'input');
inputWrap.append(title, input);

let error = document.createElement('p');
error.setAttribute('id', 'errorId');
error.setAttribute('class', 'error');
inputWrap.insertAdjacentElement('beforeend', error);
error.textContent = 'Please enter correct price';

let btnClose = document.createElement('button');

input.addEventListener('focus', function () {
    input.classList.add('focused');
    input.style.color = '';
});

input.addEventListener('blur', function () {
    input.classList.remove('focused');
    if (input.value === '') return;
    else if (
        isNaN(+input.value) ||
        input.value < 0 ||
        !input.value.trim() ||
        (1 / input.value) === -Infinity) {
        if (error.classList.contains('error')) {
            error.classList.remove('error');
            input.classList.add('incorrect');
            currentPrice.textContent = '';

        }
    } else {
        if (!error.classList.contains('error')) {
            error.classList.add('error');
            input.classList.remove('incorrect');
        }
        currentPrice.textContent = `Текущая цена: \$${parseFloat(input.value)}  `;
        input.style.color = 'green';
        btnClose.setAttribute('id', 'btnClose');
        btnClose.innerHTML = `&times;`
        currentPrice.append(btnClose);
    }
});

btnClose.addEventListener('click', function () {
    if (!error.classList.contains('error')) {
        error.classList.add('error');
        input.classList.remove('incorrect')
    }
    currentPrice.textContent = '';
    input.value = '';
});