"use strict";

let form = document.querySelector('.password-form');
let password = document.getElementById('inputPass');
let confirmPass = document.getElementById('confirmPass');
let btn = document.querySelector('.btn');
let error = document.createElement('span');
error.setAttribute('id', 'error');
error.textContent = 'Нужно ввести одинаковые значения';
btn.insertAdjacentElement('beforebegin', error);

form.addEventListener('click', function (e) {
    e.preventDefault();
    if (e.target.classList.contains('fas')) {
        let iconHidden = e.target.parentElement.querySelector('[data-hidden=true]');
        iconHidden.dataset.hidden = !iconHidden.dataset.hidden;
        e.target.dataset.hidden = 'true';
        let input = e.target.parentElement.querySelector('input');
        if (iconHidden.classList.contains('fa-eye-slash')) {
            input.setAttribute('type', 'text');
        } else {
            input.setAttribute('type', 'password');
        }
    }
    if (e.target === btn) {
        confirmPassword();
    }
})

form.addEventListener('input', function (e) {
    if (
        (e.target === password || e.target === confirmPass) &&
        error.style.visibility == 'visible' &&
        password.value === confirmPass.value
    ) {
        error.style.visibility = 'hidden';
    }
});

function confirmPassword() {
    if (
        password.value === confirmPass.value &&
        password.value !== '' &&
        password.value.trim()) {
        error.style.visibility = 'hidden';
        alert('You are welcome');
        form.reset();
    } else {
        error.style.visibility = 'visible';
    }
}

