"use strict";

$(document).ready(function () {

    $('.link-menu').click(function (e) {
        e.preventDefault();
        let el = ($(e.target).attr('href')).replace('#', '');
        if (el && $('section').is('#' + el)) {
            $('html,body').animate({
                scrollTop: $('#' + el).offset().top
            }, 1000);
        } else return;
    });

    let btnUp = $('<div></div>').attr('id', 'btn-up').prependTo('body');
    let btnHideWrap = $('<div></div>').attr('id', 'btn-hide-wrap').insertAfter($('#postsId'));
    let btnHidePosts = $('<div>Hide posts</div>').attr({ 'id': 'btn-hide-posts', 'class': 'container-section' }).prependTo(btnHideWrap);

    $(window).on('scroll', function () {
        let windowHeight = document.documentElement.clientHeight;
        if (windowHeight - $(this).scrollTop() <= 0) {
            btnUp.show();
        } else btnUp.hide();
    });

    btnUp.click(function () {
        $('html,body').animate({ scrollTop: 0 }, 1000);
    });

    btnHidePosts.click(function () {
        if ($('#postsId').css('display') === 'none') {
            btnHidePosts.html('Hide posts');
        } else {
            btnHidePosts.html('Show posts');
        }
        $('#postsId').slideToggle('500');
    });
});
