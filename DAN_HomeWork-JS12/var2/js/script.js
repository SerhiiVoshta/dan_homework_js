"use strict";

let arrImg = document.querySelectorAll('.image-to-show');
let arrSrc = [];
arrImg.forEach((el, i) => {
    arrSrc[i] = el.getAttribute('src');
    el.remove();
});

let i = 0;
let timer;
let timerCountDown;

let imgWrap = document.querySelector('.images-wrapper');

let btnWrap = document.createElement('div');
btnWrap.setAttribute('class', 'buttons-wrapper');
imgWrap.after(btnWrap);

let btnStop = document.createElement('button');
btnStop.setAttribute('class', 'btn');
btnStop.textContent = 'Прекратить';
btnWrap.append(btnStop);

let btnStart = document.createElement('button');
btnStart.setAttribute('class', 'btn');
btnStart.textContent = 'Возобновить показ';
btnStop.after(btnStart);

let pWrap = document.createElement('div');
pWrap.setAttribute('class', 'countdown-wrapper');
imgWrap.insertAdjacentElement('beforebegin', pWrap);

let countdown = document.createElement('p');
countdown.setAttribute('class', 'countdown');
countdown.textContent = 3;
pWrap.insertAdjacentElement('afterbegin', countdown);

slide();


function fadeOut(el) {
    var opacity = 1;
    var timer = setInterval(function () {
        if (opacity <= 0.1) {
            clearInterval(timer);
            document.querySelector(el).style.display = "none";
        }
        document.querySelector(el).style.opacity = opacity;
        opacity -= opacity * 0.1;
    }, 10);
}


function fadeIn(el) {
    var opacity = 0.01;
    document.querySelector(el).style.display = "block";
    var timer = setInterval(function () {
        if (opacity >= 1) {
            clearInterval(timer);
        }
        document.querySelector(el).style.opacity = opacity;
        opacity += opacity * 0.1;
    }, 10);
}


function showImg() {
    let img = document.createElement('img');
    img.setAttribute('src', arrSrc[i]);
    img.setAttribute('class', 'image-to-show');
    imgWrap.insertAdjacentElement('afterbegin', img);
    if (i + 1 === arrSrc.length) {
        i = 0;
    } else {
        i++;
    }
}

function countDown() {
    setTimeout(function start() {
        if (countdown.textContent > 1) {
            countdown.textContent--;
            timerCountDown = setTimeout(start, 1000);
        } else if (timerCountDown !== null) {
            countdown.textContent = 3;
        }
    }, 1000);
}

function slide() {
    let imgShown = document.querySelector('.image-to-show');
    if (imgShown) {
        imgShown.remove();
    }
    showImg();
    countDown();
    timer = setTimeout(slide, 3000);
}

btnWrap.addEventListener('click', function (e) {
    e.preventDefault();
    if (e.target === btnStop) {
        countdown.textContent = '';
        clearTimeout(timer);
        clearTimeout(timerCountDown);
        timer = null;
        timerCountDown = null;
    } else if (e.target === btnStart && timer === null) {
        slide();
        countdown.textContent = 3;
    } else return
});
