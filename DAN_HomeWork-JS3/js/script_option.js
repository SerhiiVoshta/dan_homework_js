"use strict";

let userNum1 = prompt('введите первое число');
let userNum2 = prompt('введите второе число');
let userOper = prompt('введите арифметическую операцию: +, -, *, /');

while(
    !userNum1 ||
    !userNum2 ||
    !userNum1.trim() ||
    !userNum2.trim() ||
    isNaN(+userNum1) ||
    isNaN(+userNum2) ||
    !checkOper(userOper)) {
        userNum1 = prompt('Проверьте правильность ввода первого числа', userNum1);
        userNum2 = prompt('Проверьте правильность ввода второго числа', userNum2);
        userOper = prompt('Проверьте правильность ввода арифметической операции: +, -, *, /', userOper);
    }

    function checkOper(z){
    if(
        z ==='+' ||
        z ==='-' ||
        z ==='*' ||
        z ==='/'){
        return true;
        }
    }

function calc(x,y,oper){
    let res;
    
    if (oper === '+'){
        res = x + y;
    }
    if (oper === '-'){
        res = x - y;
    }
    if (oper === '*'){
        res = x * y;
    }
    if (oper === '/' && y !==0){
        res = x / y;
    } else if(oper === '/' && y ===0) {
        res = 'ошибка! деление на ноль'
    }
    return res;
}

console.log(`${+userNum1} ${userOper} ${+userNum2} = ` + calc(+userNum1,+userNum2,userOper));


