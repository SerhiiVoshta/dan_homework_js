"use strict";

let userNum1 = prompt('введите первое число');
let userNum2 = prompt('введите второе число');
let userOper = prompt('введите арифметическую операцию: +, -, *, /');

if(
    !userNum1 ||
    !userNum2 ||
    !userNum1.trim() ||
    !userNum2.trim() ||
    isNaN(+userNum1) ||
    isNaN(+userNum2)) {
        console.log('Error! Invalid data.')
    } else if(
        userOper ==='+' ||
        userOper ==='-' ||
        userOper ==='*' ||
        userOper ==='/'){
            console.log(calc(+userNum1,+userNum2,userOper));
        } else {
            console.log('Error! Invalid data.');
        }

function calc(x,y,oper){
    let res;

    if (oper === '+'){
        res = x + y;
    }
    if (oper === '-'){
        res = x - y;
    }
    if (oper === '*'){
        res = x * y;
    }
    if (oper === '/' && y !==0){
        res = x / y;
    } else if(oper === '/' && y ===0) {
        res = 'ошибка! деление на ноль'
    }
    return res;
}


